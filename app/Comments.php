<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = "comments";
    protected $fillable = ['user_id', 'post_id', 'comment'];

    public function post()
    {
        return $this->belongsToMany('App\Post');
    }

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
