<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ambil data postingan yang dibuat oleh user kita
        $posts = Post::where('user_id', '=', Auth::id())->get();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, [
            'content'=>'required',
            'image'=>'image|mimes:jpeg,jpg,bmp,png',
            'category'=>'required'
        ]);
        // ambil data gambar
        // jika ada gambar yg diupload, maka beri nama sesuai ketentuan, lalu masukkan ke folder img
        // jika tidak ada gambar maka null
        $gambar = $request->file('image');
        if($gambar){
            $name_img = 'img/' . time().'-'.$gambar->getClientOriginalName();
            $gambar->move('img', $name_img);
        }
        else{
            $name_img = null;
        }

        // buat model post
        $post = new Post([
            'content' => $request->content,
            'image' => $name_img,
            'category' => $request->category
        ]);

        // ambil data user kita
        $user = Auth::user();
        // attach post ke user kita. sehingga post tersebut merupakan postingan yg kita buat
        $user->post()->save($post);
        
        return redirect('post')->with('success', 'Create post succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findorfail($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findorfail($id);
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request, [
            'content'=>'required',
            'image'=>'mimes:jpeg,bmp,png',
            'category'=>'required'
        ]);
        
        // cari model post dg id yg ingin diubah
        $post = post::findorfail($id);

        // cek apakah ada gambar postingan yang diunggah
        // jika tidak ada, maka di cek. jika sebelumnya sudah ada gambar postingan, maka gunakan yg sebelumnya.
        // jika ada, maka di cek. jika sebelumnya sudah ada gambar postingan, maka hapus terlebih dahulu file lama. dan perbarui
        if (empty($request->file('image'))){
            if($post->image){
                $post->image = $post->image;
            }
            $post_data = [
                'content'=>$request->content,
                'category'=>$request->category
            ];
        }
        else {
            if($post->image){
                unlink($post->image);
            }

            $image = $request->file('image');
            $image_name = 'img/' . time() . '-' . $image->getClientOriginalName();
            $image->move('img', $image_name);

            $post_data = [
                'content'=>$request->content,
                'image'=> $image_name,
                'category'=>$request->category
            ];
        }
        // update data post
        $post->update($post_data);

        // ambil data user kita dan simpan perubahan post yg berelasi dengan user
        $user = Auth::user();
        $user->post()->save($post);

        return redirect('/post')->with('success', 'Succesfully update post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findorfail($id);
        $post->delete();

        return redirect('/post')->with('success', 'Succesfully delete post');
    }
}
