<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __contstruct(){
        $this->middleware('auth');
    }

    public function index(){
        // cari profil user kita
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        
        return view('profile.index', ['profile' => $profile]);
    }

    public function edit($id){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.edit', ['profile' => $profile]);
    }

    public function update($id, Request $request){
        $this -> validate($request, [
            'name'=>'required',
            'username'=>'required',
            'email'=>'required',
            'profile_pict' => 'image|mimes:jpeg,jpg,bmp,png',
        ]);

        // cari profil user kita. jika sudah masukkan ke model
        $profile = Profile::find($id);
        $profile->user->name = $request['name'];
        $profile->user->username = $request['username'];
        $profile->user->email = $request['email'];
        $profile->date_of_birth = $request['birth_date'];
        $profile->city = $request['city'];
        $profile->bio = $request['bio'];
        
        // cek apakah ada profile picture yang diunggah
        // jika tidak ada, maka di cek. jika sebelumnya sudah ada profile picture, maka gunakan yg sebelumnya.
        // jika ada, maka di cek. jika sebelumnya sudah ada profile picture, maka hapus terlebih dahulu file lama. dan perbarui
        if(empty($request->file('profile_pict'))){
            if($profile->image){
                $profile->image = $profile->image;
            }
        }
        else{
            if($profile->image){
                unlink($profile->image);
            }

            $image = $request->file('profile_pict');
            $image_name = $image->getClientOriginalName();

            $profile->image = 'img/profile-photos/' . $image_name;
            $image->move('img/profile-photos', $image_name);
        }
        // update profil
        $profile->update();

        // simpan profil yg berelasi dengan user
        $user = Auth::user();
        $user->profile()->save($profile);

        return redirect('profile')->with('success', 'Profile successfully updated!');
    }
}
