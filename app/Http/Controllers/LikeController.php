<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // like postingan
    public function store(Request $request){
        $this->validate($request, [
            'post_id' => 'required'
        ]);
        $post_id = $request['post_id'];

        // cari model post yang ingin dilike
        $post = Post::find($post_id);

        // dapatkan user kita
        $user = Auth::user();
        // attach post sebagai post yg kita like
        $user->likes_post()->attach($post);

        return redirect('home');
    }

    // unlike postingan
    public function destroy($id){
        // cari model post yang ingin di unlike
        $post = Post::find($id);

        // dapatkan user kita
        $user = Auth::user();
        // detach dari post yang kita sukai
        $user->likes_post()->detach($post);

        return redirect('home');
    }
}
