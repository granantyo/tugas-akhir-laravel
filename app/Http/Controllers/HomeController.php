<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // tampilkan semua post
    public function index()
    {
        // ambil data semua post diurutkan berdasarkan updated_at
        $posts = Post::orderBy('updated_at', 'desc')->get();

        // cari tau post apa saja yg pernah user (kita) like
        // berguna untuk mengetahui bisa like/unlike post
        $user = User::find(Auth::id());
        $user_likes_post = $user->likes_post;

        return view('home', compact('posts', 'user_likes_post'));
    }
}
