<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MyProfileController extends Controller
{
    public function __contstruct(){
        $this->middleware('auth');
    }

    // menampilkan follower user kita
    public function follower(){
        // ambil data follower
        $user = User::find(Auth::user()->id);
        $myFollower = $user->followed;

        // ambil data following kita
        // berguna untuk menentukan tombol yang muncul akan follow/unfollow
        $user_has_follows = $user->follows;
        
        return view('profile.follower', ['myFollower' => $myFollower, 'user_has_follows' => $user_has_follows]);
    }

    // menampilkan user yang kita follow
    public function following(){
        // ambil data following
        $user = User::find(Auth::user()->id);
        $myFollowing = $user->follows;
        
        return view('profile.following', ['myFollowing' => $myFollowing]);
    }
}