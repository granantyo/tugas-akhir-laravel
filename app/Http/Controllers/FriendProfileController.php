<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FriendProfileController extends Controller
{
    public function __contstruct(){
        $this->middleware('auth');
    }
    
    // pencarian user lain
    public function search(Request $request){
        $search = $request['search'];

        // cari user dengan nama yg mengandung kata kunci
        $users = User::where('name', 'like', '%'.$search.'%')->get();

        return view('profile.search', ['users' => $users, 'keyword' => $search]);
    }

    // menampilkan profile user lain
    public function show($username){
        // mendapatkan user dan profil yang ingin ditampilkan
        $user = User::where('username', $username)->first();
        $profile = Profile::where('user_id', $user->id)->first();

        // cek apakah logged in user sudah follow user yang ingin ditampilkan
        $checkUserHasFollows = User::find(Auth::id())->follows;
        if($checkUserHasFollows->isEmpty()){
            $userHasFollows = 0;
        }
        else{
            foreach ($checkUserHasFollows as $follows){
                if($follows->id == $user->id){
                    $userHasFollows = 1;
                }
                else{
                    $userHasFollows = 0;
                }
            }
        }        
        return view('profile.showFriend', ['profile' => $profile, 'userHasFollows' => $userHasFollows]);
    }

    // follow user lain
    public function follow(Request $request){
        // cari user lain
        $friend_id = $request['friend_id'];
        $friend_user = User::find($friend_id);

        // dapatkan user kita
        $user = Auth::user();

        // attach sebagai following kita
        $user->follows()->attach($friend_user);

        return redirect()->back()->with('success', 'Now you are following ' . $friend_user->name);
    }

    // unfollow user lain
    public function unfollow(Request $request){
        //cari user lain
        $friend_id = $request['friend_id'];
        $friend_user = User::find($friend_id);

        // dapatkan user kita
        $user = Auth::user();

        // detach dari following kita
        $user->follows()->detach($friend_user);

        return redirect()->back()->with('success', 'You have unfollowed ' . $friend_user->name);
    }
}
