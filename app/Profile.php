<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ['image', 'date_of_birth', 'city', 'bio'];

    // user berelasi one to one dengan profil
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
