<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Comments;
use App\Likes;

class Post extends Model
{
    protected $table = "posts";
    protected $fillable = ['content', 'image', 'category'];

    // post berelasi many to one dengan user
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    // post berelasi many to many dengan user membuat tabel comment
    public function comments(){
        return $this->belongsToMany('App\User', 'comments', 'post_id', 'user_id')
                    ->withPivot('comment')
                    ->withTimestamps();
    }

    // post berelasi many to many dengan user membuat tabel like_post
    public function likes(){
        return $this->belongsToMany('App\User', 'likes_post', 'post_id', 'user_id')
                    ->withTimestamps();
    }
}
