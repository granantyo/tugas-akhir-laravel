<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // user berelasi one to one dengan profil
    public function profile(){
        return $this->hasOne('App\Profile');
    }

    // user berelasi one to many dengan post
    public function post(){
        return $this->hasMany('App\Post');
    }

    // user berelasi many to many dengan post membuat tabel likePost
    public function likes_post(){
        return $this->belongsToMany('App\Post', 'likes_post', 'user_id', 'post_id')
                    ->withTimestamps();
    }

    // user berelasi many to many dengan user membuat tabel follows
    // ini untuk mem follow
    public function follows(){
        return $this->belongsToMany('App\User', 'follows', 'my_user_id', 'friend_user_id')
                    ->withTimestamps();
    }

    // user berelasi many to many dengan user membuat tabel follows
    // ini untuk ter follow
    public function followed(){
        return $this->belongsToMany('App\User', 'follows', 'friend_user_id', 'my_user_id')
                    ->withTimestamps();
    }

    // user berelasi many to many dengan post membuat tabel comment
    public function comments(){
        return $this->belongsToMany('App\Post', 'comments', 'user_id', 'post_id')
                    ->withPivot('comment')
                    ->withTimestamps();
    }
}
