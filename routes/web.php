<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// crud post
Route::resource('post', 'PostController');

// create comment
Route::resource('comments', 'CommentsController');

// create dan delete like (like dan unlike)
Route::resource('like', 'LikeController');

// read dan update my profile
Route::resource('profile', 'ProfileController');

// mencari user lain
Route::post('/search','FriendProfileController@search');

// follow dan unfollow user lain
Route::post('/follow','FriendProfileController@follow');
Route::delete('/unfollow', 'FriendProfileController@unfollow');

// read profile user lain
Route::get('/@{username}', 'FriendProfileController@show');

// read data my follower dan my following
Route::get('/my-follower', 'MyProfileController@follower');
Route::get('/my-following', 'MyProfileController@following');

Route::get('/home', 'HomeController@index');