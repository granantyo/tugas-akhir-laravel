## Tugas Akhir Sanbercode Laravel.

### Kelompok 12
Anggota Kelompok
1. Kholishotul Amaliah
2. Granantyo Tito Prahasto
3. Restu Rizki Zakaria

### Tema Project
Topik Social Media

### Link Demo Youtube:
https://youtu.be/lTsfa3G3C0w

### Link Deployed Web App on Heroku
http://mighty-anchorage-11073.herokuapp.com/

### Spesifikasi Tugas :
- ERD
- Migrations
- Model + Eloquent
- Controller
- View (Blade)
- CRUD (Create Read Update Delete)
- Eloquent Relationships
- Laravel + Library/Packages

### Fitur Utama:
1. User harus terdaftar di web untuk menggunakan layanan sosmed
2. User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote.
3. User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri.
4. Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya.
5. Seorang User dapat meng-unfollow user lain yang telah di-follow sebelumnya.
6. Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan.
7. User dapat meng-unlike postingan yang telah di-like sebelumnya.
8. Satu postingan dapat memiliki banyak komentar. banyak komentar dimiliki oleh satu postingan.
8. Seorang User dapat mengubah profile nya sendiri.
9. Pada halaman home menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like.
10. Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)

### Entity Relationship Diagram

<img src="public/img/erd_sosmed.png">

### Web Page

<img src="public/img/landingpage.png">
<img src="public/img/homepage2.png">
