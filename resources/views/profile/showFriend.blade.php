@extends('layouts.master')

@section('title')
{{ $profile->user->name . "'" }}s Profile
@endsection

@push('styles')
<!-- Sweet Alert Css -->
<link href="{{ asset('admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row clearfix">
    <div class="card profile-card m-l-75 m-r-75">
        <div class="profile-header">&nbsp;</div>
        <div class="profile-body">
            <div class="image-area">
                <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                @if ($profile->image)
                <img src="{{ asset($profile->image) }}" width="150" height="150" alt="User" />
                @else
                <img src="{{ asset('admin/images/user.png') }}" width="150" height="150" alt="User" />
                @endif
            </div>
            <div class="content-area">
                <h3>{{ $profile->user->name }}</h3>
                <p>{{ '@' . $profile->user->username }}</p>
            </div>
        </div>
        <div class="profile-footer">
            <div class="row text-center m-b-20">
                <div class="col-sm-4">
                    <dt>Posts</dt>
                    <dd>{{ $profile->user->post->count() }}</dd>
                </div>
                <div class="col-sm-4">
                    <dt>Following</dt>
                    <dd>{{ $profile->user->follows->count() }}</dd>
                </div>
                <div class="col-sm-4">
                    <dt>Followers</dt>
                    <dd>{{ $profile->user->followed->count() }}</dd>
                </div>
            </div>
            <!-- cek apakah user sudah follow, jika sudah maka muncul button unfollow, jika belum muncul button follow -->
            @if($userHasFollows == 1)
                <form action="unfollow" method="post">
                @method('DELETE')
                @csrf
                    <input type="hidden" id="friend_id" name="friend_id" value="{{ $profile->user->id }}">
                    <button class="btn btn-secondary btn-lg waves-effect btn-block" type="submit">UNFOLLOW</button>
                </form>
                @else
                <form action="follow" method="post">
                @csrf
                    <input type="hidden" id="friend_id" name="friend_id" value="{{ $profile->user->id }}">
                    <button class="btn btn-primary btn-lg waves-effect btn-block" type="submit">FOLLOW</button>
                </form>
            @endif
        </div>
        <hr>
        <div class="row p-l-50 p-r-50">
            <dt class="col-sm-4">Email</dt>
            <dd class="col-sm-8">{{ $profile->user->email }}</dd><br><br>
            <dt class="col-sm-4">Date of Birth</dt>
            <dd class="col-sm-8">{{ $profile->date_of_birth }}</dd><br><br>
            <dt class="col-sm-4">City</dt>
            <dd class="col-sm-8">{{ $profile->city }}</dd><br><br>
            <dt class="col-sm-4">Bio</dt>
            <dd class="col-sm-8">{{ $profile->bio }}</dd><br><br>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- SweetAlert Plugin Js -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif
@endpush