@extends('layouts.master')

@section('title', 'Edit My Profile')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    EDIT MY PROFILE
                </h2>
            </div>
            <div class="body">
                <form role="form" action="/profile/{{ $profile->id }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    
                    <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                    @if (Auth::user()->profile->image)
                        <label for="profile_pict">Change Profile Picture</label><br>
                        <div class="form-group">
                            <img src="{{ asset($profile->image) }}" class="img-thumbnail" height=150px width=150px><br>
                            <input type="file" class="form-control" name="profile_pict" id="profile_pict">
                        </div>
                    @else
                        <label for="profile_pict">Add New Profile Picture</label><br>
                        <div class="form-group">
                            <input type="file" class="form-control" name="profile_pict" id="profile_pict">
                        </div>
                    @endif
                    
                    <label for="name">Full Name</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $profile->user->name) }}" placeholder="Enter your fullname">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <label for="username">Username</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="username" name="username" class="form-control" value="{{ old('username', $profile->user->username) }}" placeholder="Enter your username">
                        </div>
                        @error('username')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <label for="email">Email</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="email" id="email" name="email" class="form-control" value="{{ old('email', $profile->user->email) }}" placeholder="Enter your email">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
    
                    <label for="birth_date">Date of Birth</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="date" id="birth_date" name="birth_date" class="form-control" value="{{ old('birth_date', $profile->date_of_birth) }}" placeholder="Enter your date of birth">
                        </div>
                        @error('birth_date')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <label for="city">City</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="city" name="city" class="form-control" value="{{ old('city', $profile->city) }}" placeholder="Enter your city">
                        </div>
                        @error('city')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
    
                    <label for="bio">Bio</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea name="bio" id="bio" cols="30" rows="10">{{ old('bio', $profile->bio) }}</textarea>
                        </div>
                        @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection