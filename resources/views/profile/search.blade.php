@extends('layouts.master')

@section('title')
Search User - {{ $keyword }}
@endsection

@section('content')
<div class="row clearfix">
    @foreach($users as $key => $user)
    <div class="col-xs-12 col-sm-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                @if ($user->profile->image)
                    <img src="{{ asset($user->profile->image) }}" width="150" height="150" alt="User" />
                @else
                    <img src="{{ asset('admin/images/user.png') }}" width="150" height="150" alt="User" />
                @endif
                </div>
                <div class="content-area">
                    <h3>{{ $user->name }}</h3>
                    <p>{{ '@' . $user->username }}</p>
                </div>
            </div>
            <div class="profile-footer">
                <a href="/{{'@'. $user->username }}" style="text-size: 24px">
                    Visit profile >
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection