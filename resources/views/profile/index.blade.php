@extends('layouts.master')

@section('title', 'My Profile')

@push('styles')
<!-- Sweet Alert Css -->
<link href="{{ asset('admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row clearfix">
    <div class="card profile-card m-l-75 m-r-75">
        <div class="profile-header">&nbsp;</div>
        <div class="profile-body">
            <div class="image-area">
                <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                @if (Auth::user()->profile->image)
                <img src="{{ asset(Auth::user()->profile->image) }}" width="150" height="150" alt="User" />
                @else
                <img src="{{ asset('admin/images/user.png') }}" width="150" height="150" alt="User" />
                @endif
            </div>
            <div class="content-area">
                <h3>{{ $profile->user->name }}</h3>
                <p>{{ '@' . $profile->user->username }}</p>
            </div>
        </div>
        <div class="profile-footer">
            <div class="row text-center">
                <div class="col-sm-4">
                    <dt>Posts</dt>
                    <dd>{{ $profile->user->post->count() }}</dd>
                </div>
                <div class="col-sm-4">
                    <dt>Following</dt>
                    <dd>{{ $profile->user->follows->count() }}</dd>
                </div>
                <div class="col-sm-4">
                    <dt>Followers</dt>
                    <dd>{{ $profile->user->followed->count() }}</dd>
                </div>
            </div>
        </div>
        <hr>
        <div class="row p-l-50 p-r-50">
            <dt class="col-sm-4">Email</dt>
            <dd class="col-sm-8">{{ $profile->user->email }}</dd><br><br>
            <dt class="col-sm-4">Date of Birth</dt>
            <dd class="col-sm-8">{{ $profile->date_of_birth }}</dd><br><br>
            <dt class="col-sm-4">City</dt>
            <dd class="col-sm-8">{{ $profile->city }}</dd><br><br>
            <dt class="col-sm-4">Bio</dt>
            <dd class="col-sm-8">{{ $profile->bio }}</dd><br><br>
        </div>
        <a class="btn btn-warning btn-sm m-b-30 m-l-50" href="/profile/{{ $profile->id }}/edit">Edit</a>
    </div>
</div>
@endsection
@push('scripts')
<!-- SweetAlert Plugin Js -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif
@endpush