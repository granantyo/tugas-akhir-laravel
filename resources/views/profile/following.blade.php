@extends('layouts.master')

@section('title')
My Following
@endsection

@push('styles')
<!-- Sweet Alert Css -->
<link href="{{ asset('admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    MY FOLLOWINGS
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <tbody>
                            @forelse ($myFollowing as $user)
                            <tr>
                                <td class="col-lg-10">
                                    <div class="media">
                                        <div class="media-left">
                                            <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                                            @if($user->profile->image)
                                            <img src="{{ asset($user->profile->image) }}" width="48" height="48" alt="User" />
                                            @else
                                            <img src="{{ asset('admin/images/user.png') }}" width="48" height="48" alt="User" />
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                {{ $user->name }}
                                            </h4>
                                            {{ '@' . $user->username }}
                                        </div>
                                    </div>
                                </td>
                                <td class="col-lg-2">
                                    <form action="unfollow" method="post">
                                    @method('DELETE')
                                    @csrf
                                        <input type="hidden" id="friend_id" name="friend_id" value="{{ $user->id }}">
                                        <button class="btn btn-secondary btn-lg waves-effect btn-block" type="submit">UNFOLLOW</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                                You have no following
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Sweet Alert Plugin Js -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif
@endpush