@extends('layouts.master')

@section('title', 'Add New Post')

@section('content')
<div class="row clearfix">
    <div class="col-xs-12 col-9">
        <div class="card">
            <div class="header">
                <h2>
                    ADD NEW POST
                </h2>
            </div>
            <div class="body">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Post</a></li>
                        <li role="presentation"><a href="#status" aria-controls="status" role="tab" data-toggle="tab">Status</a></li>
                        <li role="presentation"><a href="#quote" aria-controls="quote" role="tab" data-toggle="tab">Quote</a></li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="post">
                            <form action="/post" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="image">Share picture</label>
                                    <input type="file" name="image" id="image">
                                    @error('image')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" name="category" value="post">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="Enter your caption here"></textarea>
                                @error('content')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="status">
                            <form action="/post" method="post">
                                @csrf
                                <input type="hidden" name="category" value="status">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="What is your feeling today?"></textarea>
                                @error('content')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="quote">
                            <form action="/post" method="post">
                                @csrf
                                <input type="hidden" name="category" value="quote">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="Write some quotes from you"></textarea>
                                @error('content')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection