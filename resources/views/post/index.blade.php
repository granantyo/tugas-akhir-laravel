@extends('layouts.master')

@section('title', 'My Posts')

@push('styles')
<!-- JQuery DataTable Css -->
<link href="{{ asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- Sweetalert Css -->
<link href="{{ asset('admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    MY POSTS
                </h2>
            </div>
            <div class="body">
                <a class="btn btn-primary btn-sm m-b-10" href="/post/create">Add New Post</a>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th class="col-md-2">Image</th>
                                <th class="col-md-4">Content</th>
                                <th>Number of Likes</th>
                                <th>Number of Comments</th>
                                <th>Published at</th>
                                <th class="col-md-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($posts as $post)
                            <tr>
                                <td>{{ $post->category }}</td>
                                <td>
                                @if($post->image)
                                    <img src="{{ asset($post->image) }}" class="img-responsive" height="200px" width="200px"/>
                                @endif
                                </td>
                                <td>{{ $post->content }}</td>
                                <td>{{ $post->likes->count() }}</td>
                                <td>{{ $post->comments->count() }}</td>
                                <td>{{ $post->created_at }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm mr-2" href="/post/{{ $post->id }}" style="float: left;">Show</a>
                                    <a class="btn btn-warning btn-sm mr-2" href="/post/{{ $post->id }}/edit" style="float: left;">Edit</a>
                                    <form action="/post/{{ $post->id }}" method="post" class="form-inline" style="float: left;">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                            @empty
                                No Posts yet
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('admin/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });
    });
</script>
<!-- SweetAlert Plugin Js -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif
@endpush