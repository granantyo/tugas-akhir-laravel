@extends('layouts.master')

@section('title', 'Edit Post')

@push('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="sty') }}lesheet" />
@endpush

@section('content')
<div class="row clearfix">
    <div class="col-xs-12 col-9">
        <div class="card">
            <div class="header">
                <h2>
                    EDIT POST - {{ $post->id }}
                </h2>
            </div>
            <div class="body">
                <div>
                    <form action="/post/{{ $post->id }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            {{-- <label for="category">Choose category</label> --}}
                            <select class="custom-select" name="category" id="category">
                                <option value="post">Post</option>
                                <option value="status">Status</option>
                                <option value="quote">Quote</option>
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <select class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon">
                              <option selected></option>
                              <option value="post">Post</option>
                              <option value="status">Status</option>
                              <option value="quote">Quote</option>
                            </select>
                        </div> --}}

                        <div class="form-group">
                            <label for="image">Share picture</label>
                            <input type="file" name="image" id="image">
                        </div>
                        <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="Enter your caption here">
                        {{ old('content', $post->content) }}
                        </textarea>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Select Plugin Js -->
<script src="{{ asset('admin/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush