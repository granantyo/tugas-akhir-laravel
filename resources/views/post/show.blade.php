@extends('layouts.master')

@section('title', 'Detail Post')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DETAIL POST - {{ $post->id }}
                </h2>
            </div>
            <div class="body">
                <a class="btn btn-primary btn-sm m-b-10" href="/post">Back</a>
                <div class="panel panel-default panel-post">

                    <div class="panel-heading">
                        <div class="media">
                            <div class="media-left">
                                <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                                @if($post->user->profile->image)
                                <img src="{{ asset($post->user->profile->image) }}" width="48" height="48" alt="User" />
                                @else
                                <img src="{{ asset('admin/images/user.png') }}" width="48" height="48" alt="User" />
                                @endif
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $post->user->name }}
                                </h4>
                                {{ $post->created_at }}
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        <div class="post">
                            <div class="post-heading">
                                <p>{{ $post->content }}</p>
                            </div>
                            <div class="post-content m-l-10 m-r-10">
                                @if($post->image)
                                <img src="{{ asset($post->image) }}" class="img-responsive" />
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel-footer">
                        <div>
                            <button class="btn bg-cyan waves-effect m-b-15" type="button" data-toggle="collapse" data-target="#collapseLikes" aria-expanded="false" aria-controls="collapseLikes">
                                See who likes
                            </button>
                            <div class="collapse m-b-10" id="collapseLikes">
                                <ul>
                                    @forelse($post->likes as $like)
                                        <li>{{ $like->name }}</li>
                                    @empty
                                        No one like
                                    @endforelse
                                </ul>
                            </div>
                        </div>

                        <div>
                            <button class="btn bg-cyan waves-effect m-b-15" type="button" data-toggle="collapse" data-target="#collapseComments" aria-expanded="false" aria-controls="collapseComments">
                                See who comments
                            </button>
                            <div class="collapse m-b-10" id="collapseComments">
                                @forelse($post->comments as $comment)
                                <div>
                                    <span><strong>{{ $comment->name }}</strong></span> comments :
                                    <p class="p-l-30 p-t-5">{{ $comment->pivot->comment }}</p>
                                </div>
                                <hr class="m-t-5 m-b-5">
                                @empty
                                    No one comment
                                @endforelse
                                
                                <hr>
                                <!--insert comment-->
                                <form action="/comments" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{$post->id}}" name="post_id">
                                    {{-- <input type="hidden" value="{{$post->user->id}}" name="user_id"> --}}
                                    <div class="form-group">
                                        <label for="comment">Komentar sebagai {{$post->user->name}}</label>
                                        <input type="text" class="form-control" name="comment" placeholder="Tulis komentar anda">
                                        
                                        {{-- <div class="d-flex flex-row align-items-start">
                                            <img class="rounded-circle" src="https://i.imgur.com/RpzrMR2.jpg" width="40">
                                            <textarea class="form-control bg-light ml-1 shadow-none textarea" placeholder="Tulis komentar anda"></textarea>
                                        </div> --}}

                                        @error('comment')
                                            <div class="alert alert-danger">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary">Post comment</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection