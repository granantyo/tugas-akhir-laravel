@extends('layouts.master')

@section('title', 'Home')

@push('styles')
<!-- Sweet Alert Css -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.css') }}"></script>
@endpush

@section('content')
<div class="row clearfix justify-content-center">
    <div class="col-xs-12 col-9">
        <div class="card">
            <div class="body">
                <!-- insert post -->
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Post</a></li>
                        <li role="presentation"><a href="#status" aria-controls="status" role="tab" data-toggle="tab">Status</a></li>
                        <li role="presentation"><a href="#quote" aria-controls="quote" role="tab" data-toggle="tab">Quote</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="post">
                            <form action="/post" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="image">Share picture</label>
                                    <input type="file" name="image" id="image">
                                </div>
                                <input type="hidden" name="category" value="post">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="Enter your caption here"></textarea>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="status">
                            <form action="/post" method="post">
                                @csrf
                                <input type="hidden" name="category" value="status">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="What is your feeling today?"></textarea>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="quote">
                            <form action="/post" method="post">
                                @csrf
                                <input type="hidden" name="category" value="quote">
                                <textarea name="content" id="content" class="form-control" style="min-width: 100%" rows="4" placeholder="Write some quotes from you"></textarea>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Post</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end insert post -->
                <hr class="m-t-0 m-l-0 m-r-0">
                <!-- list all post -->
                <div>
                    <div class="tab-content">
                        <div>
                            @forelse($posts as $post)
                            <div class="panel panel-default panel-post">
                                <div class="panel-heading">
                                    <div class="media">
                                        <div class="media-left">
                                            <!-- cek apakah punya profile picture, kalo tidak punya pake default -->
                                            @if($post->user->profile->image)
                                            <img src="{{ asset($post->user->profile->image) }}" width="48" height="48" alt="User" />
                                            @else
                                            <img src="{{ asset('admin/images/user.png') }}" width="48" height="48" alt="User" />
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                {{ $post->user->name }}
                                            </h4>
                                            {{ $post->created_at }}
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="post">
                                        <div class="post-heading">
                                            <p>{{ $post->content }}</p>
                                        </div>
                                        <div class="post-content m-l-10 m-r-10">
                                            @if($post->image)
                                            <img src="{{ asset($post->image) }}" class="img-responsive" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <ul>
                                        <li>
                                            <!-- cek apakah post sudah dilike user. jika sudah icon tipe fill (jika diklik maka unlike), jika belum icon tipe outline (jika diklik maka like) -->
                                            @forelse($user_likes_post as $like)
                                                @if($like->id == $post->id)
                                                    <form action="like/{{ $post->id }}" method="post" class="form-inline">
                                                    @method('DELETE')
                                                    @csrf
                                                        <button type="submit" style="border: none; background-color: Transparent; outline: none; padding: 0">
                                                            <i class="material-icons">thumb_up</i>
                                                        </button>
                                                        <span>{{ $post->likes->count() }} Likes</span>
                                                    </form>
                                                @else
                                                    <form action="like" method="post" class="form-inline">
                                                    @csrf
                                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                                        <button type="submit" style="border: none; background-color: Transparent; outline: none; padding: 0">
                                                            <i class="material-icons-outlined">thumb_up</i>
                                                        </button>
                                                        <span>{{ $post->likes->count() }} Likes</span>
                                                    </form>
                                                @endif
                                            @empty
                                                <form action="like" method="post" class="form-inline">
                                                @csrf
                                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                                    <button type="submit" style="border: none; background-color: Transparent; outline: none; padding: 0">
                                                        <i class="material-icons-outlined">thumb_up</i>
                                                    </button>
                                                    <span>{{ $post->likes->count() }} Likes</span>
                                                </form>
                                            @endforelse
                                        </li>
                                        <li>
                                            <div class="btn-group-toggle" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-{{ $post->id }}" data-target="#collapse-{{ $post->id }}" href="#collapse-{{ $post->id }}">
                                            {{-- <a href="#collapse-{{ $post->id }}"> --}}
                                                <button type="submit" style="border: none; background-color: Transparent; outline: none; padding: 0">
                                                    <i class="material-icons">comment</i>
                                                </button>
                                                <span class="ml-1">{{ $post->comments->count() }} Comments</span>
                                            {{-- </a> --}}
                                            </div>
                                        </li>
                                    </ul>

                                    <div id="collapse-{{ $post->id }}" class="bg-light p-2 collapse">
                                        <hr class="m-t-10 m-b-10">
                                        @foreach($post->comments as $comment)
                                        <div>
                                            <span><strong>{{ $comment->name }}</strong></span> comments :
                                            <p class="p-l-30 p-t-5">{{ $comment->pivot->comment }}</p>
                                        </div>
                                        <hr class="m-t-5 m-b-5">
                                        @endforeach
                                    

                                        <!--insert comment-->
                                        <form action="/comments" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" value="{{$post->id}}" name="post_id">
                                            {{-- <input type="hidden" value="{{$post->user->id}}" name="user_id"> --}}
                                            <div class="form-group">
                                                <span>Komentar sebagai <strong>{{Auth::user()->name}}</strong></span>
                                                <input type="text" class="form-control" name="comment" placeholder="Masukkan komentar">
                                                @error('comment')
                                                    <div class="alert alert-danger">
                                                        {{$message}}
                                                    </div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-primary">Post comment</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="panel">
                                No Posts yet
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Sweet Alert Plugin Js -->
<script src="{{ asset('admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Success!", "{{ Session::get('success') }}", "success");
</script>
@endif
@endpush