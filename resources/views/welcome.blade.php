<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>SocialSMART</title>

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/LineIcons.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/slick.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/default.css') }}">

    <link rel="stylesheet" href="{{ asset('smart-opl/assets/css/style.css') }}">
</head>
<body>

    <div class="preloader">
        <div class="loader">
            <div class="ytp-spinner">
                <div class="ytp-spinner-container">
                    <div class="ytp-spinner-rotator">
                        <div class="ytp-spinner-left">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                        <div class="ytp-spinner-right">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="header-area">
        <div class="navbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="#">
                                <img src="{{ asset('smart-opl/assets/images/logo.png') }}" alt="Logo">
                            </a>
                        </nav> 
                    </div>
                </div> 
            </div> 
        </div> 
        <div id="home" class="slider-area">
            <div class="bd-example">
                <div id="carouselOne" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselOne" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselOne" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item bg_cover active" style="background-image: url({{ asset('smart-opl/assets/images/slider-1.jpg') }})">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Get in touch with your friends</h2>
                                            <ul class="carousel-btn rounded-buttons">
                                            @if(Auth::check())
                                                <li><a class="main-btn rounded-three" href="/home">HOME</a></li>
                                            @else
                                                <li><a class="main-btn rounded-three" href="{{ route('login') }}">LOGIN</a></li>
                                                <li><a class="main-btn rounded-one" href="{{ route('register') }}">REGISTER</a></li>
                                            @endif
                                            </ul>
                                        </div>
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="carousel-item bg_cover" style="background-image: url({{ asset('smart-opl/assets/images/slider-2.jpg') }})">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Connect to people around the world</h2>
                                            <ul class="carousel-btn rounded-buttons">
                                            @if(Auth::check())
                                                <li><a class="main-btn rounded-three" href="/home">HOME</a></li>
                                            @else
                                                <li><a class="main-btn rounded-three" href="{{ route('login') }}">LOGIN</a></li>
                                                <li><a class="main-btn rounded-one" href="{{ route('register') }}">REGISTER</a></li>
                                            @endif
                                            </ul>
                                        </div>
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <a class="carousel-control-prev" href="#carouselOne" role="button" data-slide="prev">
                        <i class="lni-arrow-left-circle"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselOne" role="button" data-slide="next">
                        <i class="lni-arrow-right-circle"></i>
                    </a>
                </div> 
            </div>
        </div> 
    </section>


    <script src="{{ asset('smart-opl/assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('smart-opl/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('smart-opl/assets/js/popper.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/slick.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/isotope.pkgd.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/imagesloaded.pkgd.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/jquery.magnific-popup.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/scrolling-nav.js') }}"></script>
    <script src="{{ asset('smart-opl/assets/js/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/wow.min.js') }}"></script>

    <script src="{{ asset('smart-opl/assets/js/main.js') }}"></script>
</body>
</html>
