<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title')</title>
    @include('layouts.style')
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            @yield('form-header')
        </div>
        <div class="card">
            <div class="body">
                @yield('form-body')
            </div>
        </div>
    </div>

    @include('layouts.script')
</body>

</html>