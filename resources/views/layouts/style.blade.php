<!-- Fonts -->
<link rel="stylesheet" href="{{ asset('admin/css/fonts/roboto-latin.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('admin/css/fonts/material-icon.css') }}"> -->
<link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">

<!-- Bootstrap Core Css -->
<link href="{{ asset('admin/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"> --}}


<!-- Waves Effect Css -->
<link href="{{ asset('admin/plugins/node-waves/waves.css') }}" rel="stylesheet" />

<!-- Animation Css -->
<link href="{{ asset('admin/plugins/animate-css/animate.css') }}" rel="stylesheet" />

<!-- Custom Css -->
<link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    
<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="{{ asset('admin/css/themes/all-themes.css') }}" rel="stylesheet" />