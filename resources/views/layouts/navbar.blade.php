    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">SocialSMART</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li class="m-t-15">
                        <form class="form-inline my-2 my-lg-0" action="/search" method="post">
                        @csrf
                            <input type="search" name="search" class="form-control mr-sm-2" placeholder="Search name ..." value="{{ old('search') }}">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </li>
                    <!-- #END# Call Search -->
                </ul>
            </div>
        </div>
    </nav>